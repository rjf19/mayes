import pyglet, mayesSounds, mayesControls

lev = []
currentLevel = -1
bounds = (0,0)
goal = (-1,-1)
pcStart = (-1,-1)
dirs = {"r":(1,0),"l":(-1,0),"u":(0,-1),"d":(0,1)}


def initLevel(no):
	print("initializing level " + str(no))
	global lev, goal, bounds, pcStart, currentLevel
	lev = []
	currentLevel = no
	lvf = open("../levels/lv"+str(no)+".txt")
	stro = ""

	x = 0
	y = 0
	for line in lvf:
		x = 0
		layer = []
		for ch in line:
			if ch == "%":
				pcStart = (x,y)
				layer.append(" ")
			elif (ch!="\n"):
				layer.append(ch)
			if ch == "$":
				goal = (x,y)

			x+=1
		lev.append(layer)
		y+=1
	bounds = (len(lev[0]),len(lev))
	mayesSounds.levelIntro(currentLevel)

def getCurrentLevel():
	return currentLevel

def getPcStart():
	if not lev:
		initLevel(1)
		print("forcing level 1")
	return pcStart

def validPos(pos):
	if not lev:
		raise Exception("level not initialized")
	if check(pos) == " " or check(pos) == "$":
		return True

def check(pos):
	if (pos[0] < 0 or pos[1] < 0):
		return False
	if (pos[0] >= bounds[0] or pos[1] >= bounds[1]):
		return False
	return lev[pos[1]][pos[0]]

def ranger(pos,dir):
	dirc = dirs[dir]
	dist = 0
	while check(pos) == " ":
		pos = (pos[0]+dirc[0],pos[1]+dirc[1])
		dist+=1
	return dist

def endLevel():
	# print(currentLevel)
	mayesSounds.levelComplete(currentLevel)
	print("level "+str(currentLevel)+" complete, next level")
	mayesControls.extraDisable()


def nextLevel(level):
	initLevel(level)
	mayesControls.extraEnable()


def getExit():
	return goal
	
if __name__ == "__main__":
	initLevel(1)
	print(ranger(getPcStart(),"u"))