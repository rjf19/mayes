
import pyglet
from pyglet.window import key
pyglet.options['audio'] = ('openal', 'directsound', 'silent')
import pyglet.media
import mayesUtil, mayesCharacters, mayesSounds, mayesLevel, mayesControls

window = pyglet.window.Window()
mayesSounds.initSounds()
mayesLevel.initLevel(1)

pc = mayesCharacters.pc()
controls_enabled = True


@window.event
def on_key_press(symbol, modifiers):
    dist = 0
    b = True
    print (mayesControls.getEnabled())
    if mayesControls.getEnabled():
        if (symbol == key.W):
            pc.move("u")
        elif (symbol == key.A):
            pc.move("l")
        elif (symbol == key.D):
            pc.move("r")
        elif (symbol == key.S):
            pc.move("d")
        else:
            b = False
        if (symbol == key.E):
            pc.snap()
        elif (symbol == key.Q):
            pc.ping()
            #     if b:
            # mayesControls.disableTime(.2)




@window.event
def on_draw():
    window.clear()

pyglet.app.run()