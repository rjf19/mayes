import pyglet, mayesUtil, mayesControls
pyglet.options['audio'] = ('openal', 'directsound', 'silent')
import pyglet.media
ranges = []
quietDurs = []
sounds = {}
maxRange = 0
genPlayer = pyglet.media.Player()
genPlayer2 = pyglet.media.Player()
rangefinder = pyglet.media.Player()
narrator = pyglet.media.Player()
tutorialer = pyglet.media.Player()
dirs = {"r":(1,0),"l":(-1,0),"u":(0,-1),"d":(0,1)}
snappable = True
pingable = True

@tutorialer.event
def on_player_eos():
    mayesControls.extraEnable()

def cooldown(thing,dur):
    global snappable,pingable
    if thing =="snap":
        snappable = False
    if thing == "ping":
        pingable = False
    pyglet.clock.schedule_once(reenable,dur,thing)

def reenable(dt, thing):
    global snappable,pingable
    if thing == "snap":
        snappable = True
    if thing == "ping":
        pingable = True

def tutorialize(tutorial):
    mayesControls.extraDisable()
    source = pyglet.media.load("../voicelines/tutorial/"+tutorial+".wav", streaming = False)
    tutorialer.queue(source)
    tutorialer.play()



def initNarrator():
    soundList = ["level1_intro","level1_complete","level2_intro","level2_complete","level_unknown_intro","level_unknown_complete"]
    for sound in soundList:
        sounds[sound] = pyglet.media.load("../voicelines/"+sound+".wav", streaming = False)

def initSounds(max=20):
    initNarrator()
    maxRange = max
    for f in range(max+1):
        freq = 1200/(f+2)
        wf = pyglet.media.procedural.Sine(100,frequency = freq,sample_rate = 44100, sample_size = 16)
        ranges.append(wf)
        wf = pyglet.media.procedural.Silence(f/8)
        quietDurs.append(wf)
    soundList = ["oof","wall_thud","wall_tap","snap","exit_tap","step","miss","radar","win","echo_snap"]
    for sound in soundList:
        sounds[sound] = pyglet.media.load("../sounds/"+sound+".wav", streaming = False)


def snap(dist, dir):
    if snappable:
        genPlayer.next_source()
        genPlayer.queue(sounds["echo_snap"])
        genPlayer.play()
        distdir = (dist,dir)
        pyglet.clock.schedule_once(snap2, (dist/8), distdir)
        cooldown("snap",1)

def snap2(dt, distdir):
    dist, dir = distdir
    dir = dirs[dir]
    relpos = (dir[0]*4,dir[1]*4,0)
    rangefinder.next_source()
    rangefinder.queue(sounds["echo_snap"])
    rangefinder.position = relpos
    rangefinder.play()


def ping(loc, pinged):
    if pingable:
        relpos = (pinged[0]-loc[0],pinged[1]-loc[1],0)
        genPlayer.next_source()
        genPlayer.queue(sounds["radar"])
        genPlayer.play()
        genPlayer2.position = relpos
        dist = mayesUtil.euDist(loc,pinged)
        pyglet.clock.schedule_once(ping2,dist/8)
        mayesControls.disable()
    cooldown("ping",3)

def ping2(dt):
    genPlayer2.next_source()
    genPlayer2.queue(sounds["exit_tap"])
    genPlayer2.play()
    mayesControls.enable()

def playSound(sound):
	if sound in sounds:
		genPlayer.next_source()
		genPlayer.queue(sounds[sound])
		genPlayer.play()
	else:
		print("whoops lol sound not found")


def oof():
    genPlayer.next_source()
    # genPlayer2.next_source()

    genPlayer.queue(sounds["oof"])
    # genPlayer2.queue(sounds["wall_thud"])mayesLevel.getCurrentLevel()

    genPlayer.play()
	# genPlayer2.play()

def levelIntro(level):
    narrator.next_source()
    if "level"+str(level)+"_intro" in sounds:
        narrator.queue(sounds["level"+str(level)+"_intro"])
        narrator.play()
    else:
        narrator.queue(sounds["level_unknown_intro"])
        narrator.play()

def levelComplete(level):
    narrator.next_source()
    if "level"+str(level)+"_intro" in sounds:
        narrator.queue(sounds["level"+str(level)+"_complete"])
        narrator.play()
    else:
        narrator.queue(sounds["level_unknown_complete"])
        narrator.play()


	


if __name__ == "__main__":
	initSounds()
	oof()
	rangetone(1)
	
