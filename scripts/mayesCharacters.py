import mayesLevel, mayesUtil, mayesSounds, mayesControls, time
import pyglet

def nextLevel(dt,newLevel, me):
	mayesLevel.nextLevel(newLevel)
	mayesLevel.getCurrentLevel()
	me.pos = mayesLevel.getPcStart()


class pc:

	def __init__(self,id = 0):
		self.id = id
		self.pos = mayesLevel.getPcStart()
		self.dir = "r"
		self.doneYet = {"oof":False,"move":False,"snap":False,"ping":False}

	def move(self, dir):
		self.dir = dir
		newPos = mayesUtil.moveDir(self.pos, dir)
		mayesSounds.playSound("step")
		if mayesLevel.validPos(newPos):
			self.pos = newPos
			if not self.doneYet["move"] and mayesControls.tutorialEnabled():
				self.doneYet["move"] = True
				mayesSounds.tutorialize("move")
		else:
			mayesSounds.oof()
			if not self.doneYet["oof"] and mayesControls.tutorialEnabled():
				self.doneYet["oof"] = True
				mayesSounds.tutorialize("oof")

		if (self.pos == mayesLevel.getExit()):
			self.win()
		print(self.pos)
		


	def interact(self):
		spot = mayesUtil.moveDir(self.pos,self.dir)
		print(spot)
		print(mayesLevel.getExit())
		if spot == mayesLevel.getExit():
			self.win()



	def win(self):
		mayesLevel.endLevel()
		currentLevel = mayesLevel.getCurrentLevel()
		pyglet.clock.schedule_once(nextLevel,10,currentLevel+1, self)




	def ping(self):
		exit = mayesLevel.getExit()
		mayesSounds.ping(self.pos, exit)
		if not self.doneYet["ping"] and mayesControls.tutorialEnabled():
			self.doneYet["ping"] = True
			mayesSounds.tutorialize("ping")
	def snap(self):
		dist = mayesLevel.ranger(self.pos,self.dir)
		mayesSounds.snap(dist,self.dir)

		if not self.doneYet["snap"] and mayesControls.tutorialEnabled():
			self.doneYet["snap"] = True
			mayesSounds.tutorialize("snap")

			



