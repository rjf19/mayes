
import pyglet
enabled = True
extraEnabled = True

tutorial = True

def noTutorial():
	global tutorial
	tutorial = False

def tutorialEnabled():
	return tutorial

def getEnabled():
	if extraEnabled:
		return enabled
	else:
		return False
def disable():
	global enabled
	enabled = False
def enable():
	global enabled
	enabled = True

def extraDisable():
	global extraEnabled
	extraEnabled = False
def extraEnable():
	global extraEnabled
	extraEnabled = True

def disableTime(time):
	disable()
	pyglet.clock.schedule_once(enableTime,time)

def enableTime(dt):
	enable()


