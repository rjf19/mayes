import time, math

dirs = {"r":(1,0),"l":(-1,0),"u":(0,-1),"d":(0,1)}

def staTime():
    thetime = time.time()
def stpTime():
    print("took",(time.time()-thetime))

def shiftTup(orig, change):
    x = orig[0] + change[0]
    y = orig[1] + change[1]
    z = orig[2] + change[2]
    return (x,y,z)

def moveDir(orig, dirc):
	delta = dirs[dirc]
	return (orig[0] + delta[0], orig[1] + delta[1])

def euDist(pos1,pos2):
	x = pos2[0]-pos1[0]
	x = x * x
	y = pos2[1]-pos1[1]
	y = y * y
	# print (math.sqrt(y))
	return math.sqrt(y+x)